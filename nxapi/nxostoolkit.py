'''
BETA version. Changes in some functiones yet to carry out.
'''

import requests
import json

jsonrpc_headers = {'Content-Type': 'application/json-rpc'}

payload_base = {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
              "cmd": "",
              "version": 1
            },
            "id": 1
          }

class nexus:
    version = None
    platform = None
    
    def __init__(self,ip,port):
        self.ip = "sbx-nxos-mgmt.cisco.com"
        self.port = "80"
        self.uri = 'http://{}:{}/ins'.format(ip, port)
    
    def authenticate(self,user,password):
        self.username = user
        self.password = password
    
        payload = dict(payload_base)
        payload["params"]["cmd"] = "show version"
        
        response = requests.post(self.uri, 
                         data=json.dumps(payload),
                         headers=jsonrpc_headers, 
                         auth=(self.username,self.password))
        
        response_json_d = json.loads(response.text)
        version = response_json_d["result"]["body"]["kickstart_ver_str"]
        platform = response_json_d["result"]["body"]["chassis_id"]
        #print(response.text)
        
        '''TODO: parse up/down; turn it into lower_case; unidentified interface as Unknown'''
    def get_interface_status(self,if_name):
        
        payload = dict(payload_base)
        payload["params"]["cmd"] = "show interface " + if_name
        
        response = requests.post(self.uri, 
                         data=json.dumps(payload),
                         headers=jsonrpc_headers, 
                         auth=(self.username,self.password))
        print(response.text)
    
    '''TODO: turn it into lower_case; define output for incorrect inteface name; parse output'''
    def configure_interface_desc(self,if_name,descr):
        l_payload = list()
        
        payload = dict(payload_base)
        payload["params"]["cmd"] = "conf t"
                                 
        l_payload.append(payload)
                                 
        payload = dict(payload_base)
        payload["params"]["cmd"] = "interface " + if_name 
        payload["id"] = 2
        
        l_payload.append(payload)
                                 
        payload = dict(payload_base)
        payload["params"]["cmd"] = "description " + descr
        payload["id"] = 3
                                 
        l_payload.append(payload)
        
        
        response = requests.post(self.uri, 
                         data=json.dumps(l_payload),
                         headers=jsonrpc_headers, 
                         auth=(self.username,self.password))
        print(response.text)
        
def main():
  my_n = nexus("sbx-nxos-mgmt.cisco.com","80")
  my_n.authenticate("admin","Admin_1234!")
  my_n.get_interface_status("eth1/1")
        
if __name__ == '__main__':
  main()

        