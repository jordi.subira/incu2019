
###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################
import re


def is_valid_ip(ip):
	reg = '^((25[0-5]|2[0-4][0-9]|[1]?[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|[1]?[1-9]?[0-9])$'

	res = re.match(r'^((25[0-5]|2[0-4][0-9]|[1]?[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|[1]?[1-9]?[0-9])$',ip)

	return res != None



def get_ip_class(ip):
	if is_valid_ip(ip) == False:
		return "No valid IP"

	#class A: ^(([1]?[0-2]?[0-7]|[0-9]{1,2}))\.
	'''
	1.
	11.
	99.
	111.
	101.
	100.
	80.
	127.
	'''
	res = re.match(r'^(([1]?[0-2]?[0-7]|[0-9]{1,2}))\.',ip)
	if res != None:
		return "ip" + ip + " belongs to Class A"

	#class B:^((12[8-9]|1[3-8][0-9]|19[0-1]))\.
	'''
	128.
	129.
	131.
	188.
	190.
	191.
	'''
	res = re.match(r'^((12[8-9]|1[3-8][0-9]|19[0-1]))\.',ip)
	if res != None:
		return "ip" + ip + " belongs to Class B"

	#class C: ^((19[2-9]|2[0-1][0-9]|22[0-3]))\.
	'''
	192.
	223.
	'''
	res = re.match(r'^((19[2-9]|2[0-1][0-9]|22[0-3]))\.',ip)
	if res != None:
		return "ip" + ip + " belongs to Class C"

	#class D: ^((22[4-9]|23[0-9]))\.
	'''
	224.
	239.
	'''
	res = re.match(r'^(([1]?[0-2]?[0-7]|[0-9]{1,2}))\.',ip)
	if res != None:
		return "ip" + ip + " belongs to Class D"

	#class E:^((24[0-9]|25[0-5]))\.
	'''
	240.
	255.
	'''
	res = re.match(r'^((24[0-9]|25[0-5]))\.',ip)
	if res != None:
		return "ip" + ip + " belongs to Class E"

	return "Error occurs"


p1 = "192.168.1.1"
p2 = "192.168.1"
p3 = "192.168.1.256"
p4 = "0.0.0.0"
p4 = "255.255.255.255"
p5 = "...."

print(is_valid_ip(p1))
print(is_valid_ip(p2))
print(is_valid_ip(p3))
print(is_valid_ip(p4))
print(is_valid_ip(p5))