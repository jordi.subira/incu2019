
###################################################
# Duplicate words 
# The students will have to find the duplicate words in the given text,
# and return the string without duplicates.
#
###################################################

import re


def remove_duplicates(text):

	text = text.lower()
	
	text_with_no_dup = re.sub(r'\b(\w+) (?=\1)', r'\1', text)

	return text_with_no_dup


p1 = "The the marmaid's tale"
p2 = "The the marmaid's tale tale tail"

print(remove_duplicates(p1))
print(remove_duplicates(p2))
