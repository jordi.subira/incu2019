from pymongo import MongoClient
from bson.json_util import dumps

import json

table = None

def get_mongo():
    print('Connecting')
    client   = MongoClient("127.0.0.1",27017)
    print('Connected')
    return client['incubator']

def display_row(r):
    print('------- Database Dump --------')    
    for i in r:
         print(dumps(i))
    print('------------------------------')

def insert_new_entry(d):
    table.insert(d) 

def update_entry(firstname,lastname,d):
    table.update({'firstname':firstname,'lastname':lastname},d)

def display_entries(firstname=None,lastname=None):
    if firstname == None and lastname == None:
        print('firstname or lastname required')
    elif firstname == None:
        res = table.find({'lastname':{'$regex':lastname}})
    elif lastname == None:
        res = table.find({'firstname':{'$regex':firstname}})
    else:
        res = table.find({'firstname':{'$regex':firstname},'lastname':{'$regex':lastname}})
    display_row(res)

def delete_entries(firstname=None,lastname=None):
    if firstname == None and lastname == None:
        print('firstname or lastname required')
    elif firstname == None:
        table.delete_many({'lastname':{'$regex':lastname}})
    elif lastname == None:
        table.delete_many({'firstname':{'$regex':firstname}})
    else:
        table.delete_many({'firstname':{'$regex':firstname},'lastname':{'$regex':lastname}})
    


def main_code():
    db = get_mongo()
    global table
    table = db.employees

    # Clean start, deleting everything from the table.
    table.delete_many({})

    # Inserting 5 routers into the table

    '''rows_to_insert = [{'firstname':'Jimmy','lastname':'Kimmel','email':'jimmy.kimmel@mymail.com','homephone':'985645234'},
                      {'firstname':'Jim','lastname':'Kim','email':'jimmy.kimmel@mymail.com','workphone':'123456789'},
                      {'firstname':'Jimmol','lastname':'Kimmol'},
                      {'firstname':'Steven','lastname':'Gerrard','email':'stgerrard@mymail.com'},
                      {'firstname':'Frank','lastname':'Lampard','email':'frank.lampard@mymail.com'}]

    res = table.insert_many(rows_to_insert)'''
    insert_new_entry({'firstname':'Jimmy','lastname':'Kimmel','email':'jimmy.kimmel@mymail.com','homephone':'985645234'})

    # Display all database
    print('\nDisplaying all entries in DB')
    res = table.find()
    display_row(res)

    # Update the IP address of the very important router to 2.2.2.2 and dump the DB after that
    update_entry('Jimmy','Kimmel',{'firstname':'Jim','lastname':'Kim','email':'jimmy.kimmel@mymail.com','workphone':'123456789'})


    print('\nDisplaying all entries in DB')
    res = table.find()
    display_row(res)

    # Listing only the Jim
    display_entries(lastname="Kim")

    delete_entries(lastname="Kim")

    display_entries(lastname="Kim") 

main_code()